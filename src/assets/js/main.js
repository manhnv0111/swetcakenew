$(document).ready(function() {
	if ($('.slider-top').length){
	    $('.slider-top').slick({
	        dots: false,
	        arrows:false,
	        infinite: true,
	        speed: 300,
	        slidesToShow: 1,
	        slidesToScroll: 1,
	        autoplay: true,
	        autoplaySpeed: 9000,
	        prevArrow: '<span class="slick-prev slick-arrow"><i class="fa fa-angle-left"><i></span>',
	        nextArrow: '<span class="slick-next slick-arrow"><i class="fa fa-angle-right"><i></span>',
	        responsive: [
	                {
	                  breakpoint: 480,
	                  settings: {
	                    arrows:false,
	                  }
	                }
	                // You can unslick at a given breakpoint now by adding:
	                // settings: "unslick"
	                // instead of a settings object
	              ]
	    });
	}
	// MENU RESPONSIVE
	$('.navbar-toggle, .navbar-togglem').on('click', function (e) {
	  $(this).toggleClass('open');
	  $('body').toggleClass('menuin');
	  $('.nav-wrap').slideToggle();
	});

	
});